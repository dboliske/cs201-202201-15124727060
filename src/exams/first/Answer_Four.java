// name: ShaoQiang Guo ;	course: CS201;	sec: 03
// date: Feb. 25, 2022; 	name of project: exams.first
package exams.first;

import java.util.Scanner;

public class Answer_Four {

	public static void main(String[] args) {
		// Question Four: Arrays
		// Write a program that prompts the user for 5 words and prints out any word that appears more than once. 
		// NOTE: 
		// The words should be an exact match, i.e. it should be case sensitive.
		// Your programmustuse an array.
		// Do not sort the data or use an ArrayLists.
		
		String[] Array = new String[5];
		Scanner input = new Scanner(System.in);
		for(int i=0; i<Array.length; i++)
		{
			// prompt user to input a word
			System.out.println("Please input a word: ");
			// record the word to array
			Array[i] = input.nextLine(); 
		}
		
		// statistic the word and print
		int maximum = 0;
		int index = 0;
		for(int i=0; i<Array.length; i++)
		{
			int temp = 0;
			for(int j=0; j<Array.length; j++)
			{
				if (Array[i].equals(Array[j]))
				{
					temp += 1;
				}
			} // record each word appear
			if (maximum < temp) // if maximum is smaller than temp; update maximum and index
			{
				maximum = temp;
				index = i;
			}
		}
		
		// print the statistic result
		System.out.println("\nthe words your input are: ");
		for(int i=0; i<Array.length; i++)
		{
			System.out.println(Array[i]);
		}
		if (maximum == 1)
		{
			System.out.print("\nAll words appear just 1 time.");
		}
		else {
			System.out.println("\nAnd most frequent word is " + Array[index] + "; it appears " + maximum + " times .");
		}	
	}

}
