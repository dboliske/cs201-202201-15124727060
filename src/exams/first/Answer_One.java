// name: ShaoQiang Guo ;	course: CS201;	sec: 03
// date: Feb. 25, 2022; 	name of project: exams.first
package exams.first;

import java.util.Scanner;

public class Answer_One {

	public static void main(String[] args) {
		// Question One: Data Types
		// Write a program that prompts the user for an integer, add 65 to it, convert the result to a character and print that character to the console.
		System.out.println("Please input an integer: "); // prompt
		Scanner input = new Scanner(System.in);
		try {
			int number = Integer.parseInt(input.nextLine());
			char c = (char)(number + 65); // add 65 to it and convert 
			System.out.println("The number you input is " + number + " , and the convert result is " + c + " ! ");
			
		} catch (NumberFormatException e) {
			System.out.println("Your input is not an integer !");
		}
		
		input.close(); // close scanner

	}

}
