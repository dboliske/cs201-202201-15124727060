// name: ShaoQiang Guo ;	course: CS201;	sec: 03
// date: Feb. 25, 2022; 	name of project: exams.first
package exams.first;

import java.util.Scanner;

public class Answer_Three {

	public static void main(String[] args) {
		//	Question Three: Repetition

		//  Write a program that prompts the user for an integer and then prints out a Triangle of that height and width. 
		//  For example, if the user enters 3, then your program should print the following:
		//		* * *
		//		  * *
		//		    *
		
		// prompt user to input an integer
		System.out.println("Please input an integer: ");
		Scanner input = new Scanner(System.in);
		try {
			int number = Integer.parseInt(input.nextLine());
			for (int i=0; i<number; i++)
			{
				for(int j=0; j<number; j++)
				{
					if (i > j) // when i larger than j , feed empty
					{
						System.out.print("  ");
					} else { // when i smaller than j , feed *
						System.out.print("* "); 
					}
				}
				System.out.println(); // change to next line.
			}
			
			
		} catch (NumberFormatException e){
			System.out.println("Your input is not an integer !");
		}
	
		// close scanner
		input.close();

	}

}
