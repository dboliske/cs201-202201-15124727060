// name: ShaoQiang Guo ;	course: CS201;	sec: 03
// date: Feb. 25, 2022; 	name of project: exams.first
package exams.first;

// Answer_Five

// Question Five: Objects
// Write a Java class based on the following UML diagram. 
// Include all standard methods, such as constructors, mutators, accessors, toString, and equals. 
// Additionally, implement any other methods shown in the diagram.



public class Pet {
	// Variables
	private String name;
	private int age;
	
	// default constructor
	public Pet() {
		name = "Spot_1";
		age = 1;
	}
	
	// non-default constructor
	public Pet(String name, int age)
	{
		name = "Spot_1";
		setName(name);
		age = 1;
		setAge(age);
	}
	
	// mutator method for name
	public void setName(String name)
	{
		this.name = name;
	}
	
	// mutator method for age
	public void setAge(int age)
	{
		if (age > 0)
		{
			this.age = age;
		}
	}
	
	// accessor method for name
	public String getName()
	{
		return name;
	}
	
	// accessor method for age
	public int getAge()
	{
		return age;
	}
	
	// equals method
	public boolean equals(Object obj)
	{
		if (!(obj instanceof Pet))
		{
			return false;
		}
		Pet p = (Pet)obj;		
		if (!(this.name.equals(p.name)))
		{
			return false;
		} else if (this.age != p.age)
		{
			return false;
		} else {
			return true;
		}
	}
	
	// toString
	public String toString()
	{
		return this.name + "is a pet and it's " + this.age + "years old .";
	}

}
