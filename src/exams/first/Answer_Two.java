// name: ShaoQiang Guo ;	course: CS201;	sec: 03
// date: Feb. 25, 2022; 	name of project: exams.first
package exams.first;

import java.util.Scanner;

public class Answer_Two {

	public static void main(String[] args) {
		// Question Two: Selection
		// Write a program that prompts the user for an integer. 
		// If the integer is divisible by 2 print out "foo", and if the integer is divisible by 3 print out "bar". 
		// If the integer is divisible by both, your program should print out "foobar" 
		// and if the integer is not divisible by either, then your program should not print out anything.
		
		// prompt user to input an integer
		System.out.println("Please input an integer: ");
		Scanner input = new Scanner(System.in);
		try {
			int number = Integer.parseInt(input.nextLine());
			
			if (number % 2 == 0 && number % 3 == 0) // If the integer is divisible by both
			{
				System.out.println("foobar"); // print foobar
			} else if (number % 2 == 0) // If the integer is divisible by 2
			{
				System.out.println("foo"); // print foo
			} else if (number % 3 == 0) // If the integer is divisible by 3
			{
				System.out.println("bar"); // print bar
			} else { // if the integer is not divisible by either
				// print nothing
			}
			
		} catch (NumberFormatException e) {
			System.out.println("Your input is not an integer !");
		}
		// close scanner
		input.close();
	}

}
