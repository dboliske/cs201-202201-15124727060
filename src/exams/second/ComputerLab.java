// name: ShaoQiang Guo ;	course: CS201;	sec: 03
// date: April. 29, 2022; 	name of project: exams.second
package exams.second;

public class ComputerLab extends Classroom {
	private boolean computers;
	
	// default constructor
	public ComputerLab() {
		super();
		computers = true;
	}
	
	// accessor method for computers
	public boolean hasComputers() {
		return computers;
	}
	
	// mutator method for computers
	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	// toString method
	@Override
	public String toString() {
		return super.toString() + ", hasComputers: " + computers;
	}
	

}
