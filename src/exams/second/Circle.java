//name: ShaoQiang Guo ;	course: CS201;	sec: 03
//date: April. 29, 2022; 	name of project: exams.second
package exams.second;

public class Circle extends Polygon {
	private double radius;
	
	// default constructor
	Circle(){
		super.setName("Circle");
		radius = 1;
	}
	
	// accessor method for radius
	public double getRadius() {
		return radius;
	}

	// mutator method for radius
	public void setRadius(double radius) {
		if (radius > 0) {
			this.radius = radius;
		}
	}
	
	// toString method
	@Override
	public String toString() {
		return "Name is " + super.getName() + " and radius is " + radius + " .";
	}

	// area method
	@Override
	public double area() {
		return Math.PI * (radius * radius);
	}
	
	// permeter method
	@Override
	public double perimeter() {
		return 2.0 * Math.PI * radius;
	}

}
