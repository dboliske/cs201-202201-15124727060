//name: ShaoQiang Guo ;	course: CS201;	sec: 03
//date: April. 29, 2022; 	name of project: exams.second

package exams.second;

public abstract class Polygon {
	protected String name;
	
	// default constructor
	public Polygon() {
		name = null;
	}

	// acessor method for name
	public String getName() {
		return name;
	};
	
	// mutator method for name
	public void setName(String name) {
		this.name = name;
	};
	
	// to String method
	public abstract String toString();
	
	// area method
	public abstract double area();
	
	// perimeter method
	public abstract double perimeter();
}


