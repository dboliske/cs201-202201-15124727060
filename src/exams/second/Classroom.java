// name: ShaoQiang Guo ;	course: CS201;	sec: 03
// date: April. 29, 2022; 	name of project: exams.second

package exams.second;


public class Classroom {
	protected String building;
	protected String roomNumber;
	private int seats;
	
	// default constructor
	public Classroom() {
		building = "IIT CS Building";
		roomNumber = "001";
		seats = 0;
	}
		
	// accessor method for buinding
	public String getBuilding() {
		return building;
	}
	
	// mutator method for building
	public void setBuilding(String building) {
		this.building = building;
	}
	
	// accessor method for roomnumber
	public String getRoomNumber() {
		return roomNumber;
	}
	
	// mutator method for roomNumber
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	
	// accessor method for seats
	public int getSeats() {
		return seats;
	}
	
	// mutator method for seats
	public void setSeats(int seats) {
		if (seats > 0) {
			this.seats = seats;
		}
	}
	
	// toString method
	public String toString() {
		return "building : " + building + ", roomNumber: " + roomNumber + ", seats: " + seats + " .";
	}
	
	
}
