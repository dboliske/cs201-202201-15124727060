//name: ShaoQiang Guo ;	course: CS201;	sec: 03
//date: April. 29, 2022; 	name of project: exams.second
package exams.second;

public class Rectangle extends Polygon {
	private double width;
	private double height;
	
	// default constructor
	public Rectangle() {
		super.setName("Rectangle");
		width = 1;
		height = 1;
	}
	
	// accessor method foe width
	public double getWidth() {
		return width;
	}
	
	// mutator method for width
	public void setWidth(double width) {
		if (width > 0) {
			this.width = width;
		}
	}

	// accessor method for height
	public double getHeight() {
		return height;
	}
	
	// mutator method for height
	public void setHeight(double height) {
		if (height > 0) {
			this.height = height;
		}
	}
	
	// toString method
	@Override
	public String toString() {
		return "Name is " + super.getName() + " and width is " + width + " and height is " + height + " .";
	}
		
	// area method
	@Override
	public double area() {
		return width * height;
	}
	
	// perimeter method
	@Override
	public double perimeter() {
		return 2 * (width + height);
	}

}
