//name: ShaoQiang Guo ;	course: CS201;	sec: 03
//date: April. 29, 2022; 	name of project: exams.second
package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class answerFive {
	// JumpSearch function
	public static int JumpSearch(ArrayList<Double> data, double num) {
		int step = (int)Math.sqrt(data.size());
		int index = 0;
		
		while(data.get(Math.min(step, data.size()-1)) < num) {
			index = step;
			step += (int)Math.sqrt(data.size());
			if (index > data.size()) { // not in list
				return -1;
			}
			while(data.get(index) < num) {
				index++;
				if (index == Math.min(step, data.size() - 1)) { // not in list
					break;
				}
			}
			if(data.get(index) == num) {
				return index;
			}
		}
		
		return -1;
	}
	
	// show arraylist method
	public static void Show(ArrayList<Double> dataList){
		for(int i=0; i<dataList.size(); i++) {
			System.out.print(dataList.get(i) + " ");
		}
		System.out.println();
	}
	
	
	public static void main(String[] args) {
		double[] data = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		ArrayList<Double> dataList = new ArrayList<Double>();
		for (double s: data) {
			dataList.add(s);
		}
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number you want search: ");
		double num = input.nextDouble();
		int index = JumpSearch(dataList, num);
		if(index == -1) {
			System.out.println(index);
			System.out.println("Sorry, the number is not in list . ");
		}else {
			System.out.println("Its index is " + index + " and it is " +dataList.get(index) +".");
		}
		System.out.println("All the numbers are: ");
		Show(dataList);
	}
}
