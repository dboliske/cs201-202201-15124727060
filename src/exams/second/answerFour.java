//name: ShaoQiang Guo ;	course: CS201;	sec: 03
//date: April. 29, 2022; 	name of project: exams.second
package exams.second;

import java.util.ArrayList;

public class answerFour {
	// select sorting method
	public static void SelectSorting(ArrayList<String> dataList){
		ArrayList<String> sortingData = new ArrayList<String>();
		for(int j=0; j<dataList.size()-1; j++) {
			String minstr = dataList.get(j);
			int index = j;
			for(int i=j+1; i< dataList.size(); i++) {
				if (minstr.compareTo(dataList.get(i))>0) {
					minstr = dataList.get(i);
					index = i;
				}
			}
			if (index != j) {
				swap(dataList, index, j);
			}
		}
	}
	
	// show arraylist method
	public static void Show(ArrayList<String> dataList){
		for(int i=0; i<dataList.size(); i++) {
			System.out.print(dataList.get(i) + " ");
		}
		System.out.println();
	}
	
	// swap method
	public static void swap(ArrayList<String> dataList, int index1, int index2) {
		String temp = dataList.get(index1);
		dataList.set(index1, dataList.get(index2));
		dataList.set(index2, temp);
		temp = null;
	}
	
	
	public static void main(String[] args) {
		String[] data = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		ArrayList<String> dataList = new ArrayList<String>();
		for (String s: data) {
			dataList.add(s);
		}
		System.out.println("Before sorting: ");
		Show(dataList);
		SelectSorting(dataList);
		System.out.println("After sorting: ");
		Show(dataList);
	}
}
