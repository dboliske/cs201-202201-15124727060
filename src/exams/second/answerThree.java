//name: ShaoQiang Guo ;	course: CS201;	sec: 03
//date: April. 29, 2022; 	name of project: exams.second
package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class answerThree {
	// get max number function
	public static double getMaxNumber(double number1, double number2) {
		return (number1 > number2) ? number1 : number2;
	}
	// get min number function
	public static double getMinNumber(double number1, double number2) {
		return (number1 < number2) ? number1 : number2;	
	}
	
	// show arraylist function
	public static void show(ArrayList<Double> numberlist) {
		for(double d : numberlist) {
			System.out.print(d + " ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		ArrayList<Double> numberList = new ArrayList<Double>();
		String prompt = "Please input number or done: ";
		boolean flag = true;
		double maxNumber = 0;
		double minNumber = 1e5;
		Scanner input = new Scanner(System.in);
		do {
			System.out.println(prompt);
			String echo = input.nextLine();
			if (echo.compareToIgnoreCase("done") == 0) {
				flag = false;
			}else {
				try {
					double num = Double.parseDouble(echo);
					maxNumber = getMaxNumber(maxNumber, num);
					minNumber = getMinNumber(minNumber, num);
					numberList.add(num);
				}catch(NumberFormatException e) {
					System.out.println("Sorry, your input is no number or string 'done'.");
				}
			}
		}while(flag);
		
		System.out.println("The number your input are: ");
		show(numberList);
		System.out.println("Maxnumber is " + maxNumber);
		System.out.println("Minnumber is " + minNumber);
		
		
		
	}

}
