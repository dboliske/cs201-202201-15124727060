// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Feb. 03, 2022; 	 name of project: labs.lab3
package labs.lab3;

public class Exercise3 {

	public static void main(String[] args) {
		// 3.Write a program that will find the minimum value and print it to the console for the given array:
		int[] array = new int[]{72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 
				107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 
				32, 945, 32, 8747, 32, 8899, 32, 62421};
		
		int minimum = array[0];
		for(int i=1; i< array.length; i++)
		{
			if (minimum > array[i]) // compare value
			{
				minimum = array[i];
			}
		}
		// print the minimum value
		System.out.println("the minimum value of the array is " + minimum);
		}

}
