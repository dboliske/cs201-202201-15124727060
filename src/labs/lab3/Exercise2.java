// name: ShaoQiang Guo ;	 course: CS201;	sec: 02
// date: Feb. 03, 2022; 	 name of project: labs.lab3
package labs.lab3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) throws IOException {
//		2.Write a program that will continue to prompt the user for numbers, storing them in an array, until they enter "Done" to finish,
//		then prompts the user for a file name so that these values can be saved to that file. 
//		For example, if the user enters "output.txt", then the program should write the numbers that have been read to "output.txt".
		boolean flag = true;
		double[] numbers = new double[2];
		int count = 0;
		Scanner input = new Scanner(System.in);
		do {
			
			// extend array if its length is not enough.
			if (count == numbers.length)
			{
				double[] larger = new double[numbers.length + 5]; // source length + 5
				for(int i=0; i< numbers.length; i++)
				{
					larger[i] = numbers[i];
				}
				
				numbers = larger;
				larger = null;
			}

			//prompt user input input number
			System.out.println("Please input a number you want: ");
			String number_or_done = input.nextLine();
			if (number_or_done.compareToIgnoreCase("done") == 0) // if input message is Done or done; break the loop
			{
				flag = false;
			}
			else 
			{
				numbers[count] = Double.parseDouble(number_or_done);
				count++;
			}	
		}while(flag);
		
		
		// remove extra data.
		double[] smaller = new double[count];
		for(int i=0; i < count; i++)// print user input
		{
			System.out.println("the value of index %d in array is ".formatted(i) + numbers[i]);
			smaller[i] = numbers[i];
		}
		numbers = smaller;
		smaller = null;
		
		// write array to a txt file.
		System.out.println("Please input a txt-file name :");
		String save_path = "./src/labs/lab3/" + input.nextLine() + ".txt";
		File txtfile = new File(save_path);
		txtfile.createNewFile();
		BufferedWriter out = new BufferedWriter(new FileWriter(txtfile));
		for(int i=0; i < numbers.length; i++) // write data to file.
		{
			out.write(String.valueOf(numbers[i]) + '\n');
		}
		out.flush();
		out.close();		
		input.close();
		System.out.println("Goodbye!");
	}

}
