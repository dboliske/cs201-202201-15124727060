// name: ShaoQiang Guo ;	 course: CS201;	sec: 01
// date: Feb. 03, 2022; 	 name of project: labs.lab3
package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) throws IOException {
		// 1.You have been given a file called "src/labs/lab3/grades.csv". It contains a list of students and their exam grades. 
		// Write a program that reads in the file and computes the average grade for the class and then prints it to the console.
//		File grades = new File("src/labs.lab3/grades.csv");
		// sorry, the relative path is not work.
//		File grades = new File("C:/Users/Guo/git/cs201-202201-15124727060/src/labs/lab3/grades.csv");
		File grades = new File("./src/labs/lab3/grades.csv");
		Scanner lines = new Scanner(grades);
		double total_grade = 0.0d;
		int count = 0;
		while(lines.hasNextLine())
		{
			String[] line = lines.nextLine().split(",");	// ["name", "grade"]
			System.out.println("name: " + line[0] + ";  grade: " + line[1]);
			double grade = Double.parseDouble(line[1]);
			total_grade += grade;
			count++;
		}
		// print the average grade of the class.
		System.out.println("\nthe average grade for the class is %.2f".formatted(total_grade / count));
		System.out.println("Goodbye!");
		lines.close();
	}

}
