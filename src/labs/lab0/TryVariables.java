// name: ShaoQiang Guo ;	course: CS201;	sec: 04
// date: Jan. 26, 2022; 	name of project: labs.lab0
package labs.lab0;

public class TryVariables {

	public static void main(String[] args) {
		// This example shows how to declare and initialize variables

		int integer = 100;
		long large = 42561230L;
		// small = 25;
		int small = 25;
		byte tiny = 19;

		// float f = .0925F
	    float f = .0925F;
		double decimal = 0.725;
		double largeDouble = +6.022E23; // This is in scientific notation

		char character = 'A';
		boolean t = true;

		System.out.println("integer is " + integer);
		System.out.println("large is " + large);
		// system.out.println("small is " + small);
		System.out.println("small is " + small);
		// System.out.println("tiny is " + tine);
		System.out.println("tiny is " + tiny);
		System.out.println("f is " + f);
		// System.out,println("decimal is " + decimal);
		System.out.println("decimal is " + decimal);
		System.out.println("largeDouble is " + largeDouble);
		// System.out.println("character is " character);
		System.out.println("character is " + character);
		System.out.println("t is " + t);

	}

}