// name: ShaoQiang Guo ;	course: CS201;	sec: 05
// date: Jan. 26, 2022; 	name of project: labs.lab0
package labs.lab0;


public class lab0_exercise5 {

	public static void main(String[] args) {
		// side length of the square.
		int x = 9;
		for(int i = 0; i <= x; i++) // iteration with the row of the square
		{
			for(int j = 0; j <= x; j++)// iteration with the column of the square
			{
				if (i == 0 || i == x) // the first line or the last line ; print "* "
				{
					System.out.print("* ");
				}
				else if(i == 1 || i == x - 1) // the second line or the second line from bottom; print "* "
				{
					System.out.print("* ");
				}
				else if(j == 0 || j == x) // the first column or the last column; print "* "
				{
					System.out.print("* ");
				}
				else if(j == 1 || j == x - 1) // the second column or the second column from right border; print "* "
				{
					System.out.print("* ");
				}
				else // other postion; print "  "
				{
					System.out.print("  ");
				}
			}
			// line feed after inner loop finished
			System.out.println("");
		}
	}

}
