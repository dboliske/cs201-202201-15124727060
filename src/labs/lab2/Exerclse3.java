// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Jan. 31, 2022; 	 name of project: labs.lab2
package labs.lab2;

import java.util.Scanner;

public class Exerclse3 {

	public static void main(String[] args) {
//		4.Write a Java program that will repeatedly display a menu of choices to a user and prompt them to enter an option. 
//		You should use the following options:
//			1.Say Hello - This should print "Hello" to console.
//			2.Addition - This should prompt the user to enter 2 numbers and return the sum of the two.
//			3.Multiplication - This should prompt the user to enter 2 numbers and return the product of the two.
//			4.Exit - Leave the program
		Scanner input = new Scanner(System.in);
		Scanner numbers = new Scanner(System.in);
		boolean flag = true;
		do {	// option Menu
			System.out.println("\n\n\t\t Option Menu \t\t");
			System.out.println("1. Say Hello");
			System.out.println("2. Addition");
			System.out.println("3. Multiplication");
			System.out.println("4. Exit");
			System.out.println("Select the function you want: ");
			char num = input.nextLine().charAt(0);
			
			switch(num)
			{
				case '1': // function 1
					System.out.println("Hello!");
					break;
				case '2': // function 2  a add b
					System.out.println("Please input the first number: ");
					double num1 = Double.parseDouble(numbers.nextLine());
					System.out.println("Please input the Second number: ");
					double num2 = Double.parseDouble(numbers.nextLine());
					System.out.println("The sum of these two number is : " + "%.2f".formatted(num1 + num2));
					break;
				case '3': // function 3  a multiply b
					System.out.println("Please input the first number: ");
					double num3 = Double.parseDouble(numbers.nextLine());
					System.out.println("Please input the Second number: ");
					double num4 = Double.parseDouble(numbers.nextLine());
					System.out.println("The product of these two number is : " + "%.2f".formatted(num3 * num4));
					break;
				case '4': // function 4 exit
					flag = false;
					System.out.println("Thanks for using our system .");
					break;
				default:
					System.out.println("Sorry, it's an unspecified function.");
					break;
			}			
		}while(flag);
		input.close();
		numbers.close();
		System.out.println("Goodbye!");

	}

}
