// name: ShaoQiang Guo ;	 course: CS201;	sec: 01
// date: Jan. 31, 2022; 	 name of project: labs.lab2
package labs.lab2;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		1.Write a Java program that will prompt 
//		the user for a number and print out a square with those dimensions. 
		// prompt user to input a number as dimension
		
		System.out.println("Please input a number : ");
		Scanner input = new Scanner(System.in);
		int num = Integer.parseInt(input.nextLine());
		for(int i=0; i < num; i++)
		{
			for(int j=0; j < num; j++)
			{
				System.out.print("* ");
			}
			// line feed after inner loop.
			System.out.println();
		}
		input.close();
	}

}
