// name: ShaoQiang Guo ;	 course: CS201;	sec: 02
// date: Jan. 31, 2022; 	 name of project: labs.lab2
package labs.lab2;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
//		1.Write a Java program that will prompt the user for the grades for an exam, computes the average, 
//		and returns the result. Your program must be able to handle an unspecified number of grades.
//		2.Be sure to test your code with a variety of values and number of grades.
//		3.(Hint: Tell the user to enter -1 when they finished entering grades)
		// prompt user to input grades
		Scanner input = new Scanner(System.in);
		boolean flag = true;
		double total_grade = 0d;
		int count = 0;
		do {
			System.out.println("Please input your grade: ");
			System.out.println("Tips: You can input -1 to exit our system .");
			String line = input.nextLine();
			double grade = Double.parseDouble(line);
			// I define a suitable grade area is 0-100.
			if (grade == -1)
			{
				flag = false;
				break;
			}
			else if (grade < 0 || grade > 100)
			{
				System.out.println("It's a unspectifiled grade, please check your input and try it again .");
			}
			else
			{
				total_grade += grade;
				count += 1;
			}
		}while (flag == true);
		System.out.println("total grades:  " + total_grade + ";   subjects: " + count);
		if (count == 0)
		{
			System.out.println("Sorry, you didn't input any useful grade. ");
		}
		else
		{
			// print the average of grades.
			System.out.println("The average of grades you input is %.2f".formatted(total_grade / count));
		}
		input.close();
	}

}
