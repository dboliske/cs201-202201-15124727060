// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 04, 2022; 	 name of project: labs.lab5
package labs.lab5;

public class CTAStation extends GeoLocation {
	//Variables 
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	// default constructor
	public CTAStation() {
		super();
		name = "Racine";
		location = "elevated";
		wheelchair = false;
		open = false;
	}
	
	// non-default constructor
	public CTAStation(String name, double lat, double lng, String location,
			boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = name;
		setName(name);
		this.location = location;
		setLocation(location);
		this.wheelchair = wheelchair;
		setWheelchair(wheelchair);
		this.open = open;
		setOpen(open);
	}
	
	// accessor method for name
	public String getName() {
		return name;
	}
	
	// accessor method for location
	public String getLocation() {
		return location;
	}
	
	// hasWheelchair method
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	// isOpen method
	public boolean isOpen() {
		return open;
	}
	
	// mutator method for name
	public void setName(String name) {
		this.name = name;
	}
	
	// mutator method for location
	public void setLocation(String location) {
		this.location = location;
	}
	
	// mutator method for wheelchair
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	// mutator method for open
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	// toString method
	public String toString() {
		return "name: " + name +" ("+ super.getLat() + "," + super.getLng() + "), location: " + location + ", hasWheelchiar: " + wheelchair + ", isOpen: " + open;
	}
	
	// equals method
	// public boolean equals() {
	public boolean equals(Object obj) {
		if (!(obj instanceof GeoLocation)) {
			return false;
		} 
		GeoLocation g = (GeoLocation)obj;
		if(!(super.equals(g))) {
			return false;
		}else if (!(obj instanceof CTAStation)) {
			return false;
		}
		CTAStation c = (CTAStation)obj;
		if(!(this.name.equals(c.name))) {
			return false;
		} else if(!(this.location.equals(c.location))) {
			return false;
		} else if(this.hasWheelchair()!= c.hasWheelchair()) {
			return false;
		} else if(this.isOpen() != c.isOpen()) {
			return false;
		} else {
			return true;
		}
	}
	

	
}
