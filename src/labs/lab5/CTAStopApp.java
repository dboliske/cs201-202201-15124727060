// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 04, 2022; 	 name of project: labs.lab5
package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class CTAStopApp {
	// readFile function
	public static CTAStation[] readFile(String Filename) {
		CTAStation[] stations = new CTAStation[5];
		int count = 0;
		try {
			File file = new File(Filename);
			Scanner file_reader = new Scanner(file);
			while(file_reader.hasNextLine()) {
				String line = file_reader.nextLine();
				if (line.split(",")[0].equals("Name")) { // pass the first line
					continue;
				} else {
					// Name,Latitude,Longitude,Location,Wheelchair,Open
					String[] values = line.split(",");
					String name = values[0];
					double lat = Double.parseDouble(values[1]);
					double lng = Double.parseDouble(values[2]);
					String location = values[3];
					boolean wheelchair = Boolean.parseBoolean(values[4]);
					boolean open = Boolean.parseBoolean(values[5]);
					CTAStation cta = new CTAStation(name, lat, lng, location, wheelchair, open);
					// System.out.println("------>   " + cta.toString());
					if (count == stations.length) { //expand array 
						stations = resize(stations, count * 2);
					}
					stations[count] = cta;
					count++;
				}
			}
			// drop redundancy array
			stations = resize(stations, count);
		} catch (FileNotFoundException e) {
			System.out.println("the filepath doesn't exist; please check filepath your input.");
		} catch (IOException e) {
			System.out.println("Unable to read!");
		} catch (Exception e) {
			System.out.println("Sorry, someting wrong when reading file.");
			System.out.println(e.getMessage());
		}
		return stations;
	}
		
	// resize array function
	public static CTAStation[] resize(CTAStation[] stations, int count)
	{
		CTAStation[] temp = new CTAStation[count];
		int limit = (stations.length < count) ? stations.length:count;
		for(int i=0; i<limit; i++) {
			temp[i] = stations[i];
		}
		stations = temp;
		temp = null;
		return stations;
	}
		
	// Menu function
	public static void menu(CTAStation[] stations) {
		Scanner input = new Scanner(System.in);
		boolean flag = true;
		do {
			System.out.println("\n1.Display Station Names");
			System.out.println("2.Display Stations with/without Wheelchair access");
			System.out.println("3.Display Nearest Station");
			System.out.println("4.Exit");
			System.out.println("choice:");
			String c = input.nextLine();
			switch (c) {
				case "1":
					// Display Station Names
					displayStationNames(stations);
					break;
				case "2":
					// Display Stations with/without Wheelchair access
					displayByWheelchair(input, stations);
					break;
				case "3":
					// Display Nearest Station
					displayNearest(input, stations);
					break;
				case "4":
					// Exit
					flag = false;
					break;
				default:
					System.out.println("Sorry, it not a support choice.");			
			}		
		} while(flag);
		
		
		System.out.println("Goodbye!");
		input.close();
	}
	
	// displayStationNames function
	public static void displayStationNames(CTAStation[] stations) {
		// Iterates through CTAStation[] and prints the names of the stations
		for (int i=0; i<stations.length; i++)
		{
			System.out.println("name: " + stations[i].getName());
		}
	}
	
	// displayByWheelchair function
	public static void displayByWheelchair(Scanner input, CTAStation[] stations) {
		// prompt user input 'y' or 'n'
		System.out.print("With or Without Wheelchair(input 'y' or 'n':)");
		boolean flag = false;
		boolean hasWheelchair = false;
		do {		
			char c = input.nextLine().charAt(0);
			switch (c) {
				case 'y':
					hasWheelchair = true;
					flag = true;
					break;
				case 'n':
					flag = true;
					break;
				default:
					System.out.println("Sorry, check your input and input again, please");
			}					
		} while(!flag);
		
		// loop and display
		boolean empty = true;
		int count = 0;
		for (int i=0; i<stations.length; i++)
		{
			if (stations[i].hasWheelchair() == hasWheelchair) {
				System.out.println(stations[i].toString());
				empty = false;
				count++;
			}
		}
		// Display a message if nostations are found
		if(empty) {
			System.out.println("Sorry , no stations found.");
		}else {
			System.out.println(count + " stations found.");
		}
	}
	
	// displayNearest function
	public static void displayNearest(Scanner input, CTAStation[] stations) {
		//Prompts the user for a latitude and longitude
		boolean flag =true;
		double[] pos = new double[2];
		do {
			try {
				System.out.println("please input latitude: ");
				pos[0] = Double.parseDouble(input.nextLine()); //lat
				System.out.println("please input longitude: ");
				pos[1] = Double.parseDouble(input.nextLine()); //lng
				flag = false;
			}catch(Exception e) {
				System.out.println("wrong data-type, please check it and input again.");
			}		
		}while(flag);
		
		//loop , compute distance and find nearest station
		double mini_distance = 1e6;
		int index = 0;
		for(int i=0; i<stations.length; i++) {
			if (mini_distance > stations[i].calcDistance(pos[0], pos[1])) {
				mini_distance = stations[i].calcDistance(pos[0], pos[1]);
				index = i;
			}
		}
		System.out.println("the nearest station is:\n " + stations[index].toString());
		System.out.println("the dsitance is " + mini_distance);		
	}
	
	// main
	public static void main(String[] args) {	
		// String filename = "./src/labs/lab5/unknown.csv";	
		String filename = "./src/labs/lab5/CTAStops.csv";		
		CTAStation[] filearray = readFile(filename);
		if (filearray[0] != null) {
			menu(filearray);
		}
	}

}
