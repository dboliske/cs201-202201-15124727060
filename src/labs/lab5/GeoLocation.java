// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 04, 2022; 	 name of project: labs.lab5

package labs.lab5;

public class GeoLocation {
	// Variables
	private double lat;
	private double lng;
	
	// default constructor
	public GeoLocation()
	{
		lat = 0.0;
		lng = 0.0;
	}
	
	// non-default constructor
	public GeoLocation(double lat, double lng)
	{
		this.lat = 0.0;
		setLat(lat);
		this.lng = 0.0;
		setLng(lng);
	}
	
	// accessor method for lat
	public double getLat()
	{
		return lat;
	}
	
	// accessor method for lng
	public double getLng()
	{
		return lng;
	}
	
	// mutator method for lat
	public void setLat(double lat)
	{
		// between -90 and +90
		if(lat <= 90 && lat >= -90) {
			this.lat = lat;
		}
	}
	
	// mutator method for lng
	public void setLng(double lng)
	{
		if(lng <= 180 && lng >= -180) {
			this.lng = lng;
		}
	}
	
	// toString method
	public String toString()
	{
		return "(%s , %s)".formatted(lat, lng);
	}
	
	// validLat method
	public boolean vaildLat(double lat)
	{
		if (lat >= -90 && lat <= 90)
		{
			return true;
		}
		return false;
	}
	
	// validlng method
	public boolean validLng(double lng)
	{
		if (lng >= -180 && lng <= 180)
		{
			return true;
		}
		return false;
	}
	
	// equals method
	public boolean equals(GeoLocation g)
	{
		if (this.lat != g.lat) {
			return false;
		} else if(this.lng != g.lng) {
			return false;
		} else {
			return true;
		}
	}
	
	//calcDistance method with instance variable
	public double calcDistance(GeoLocation g) {
		// Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lng1 - lng2, 2))
		return Math.sqrt(Math.pow(this.lat - g.lat, 2) + Math.pow(this.lng - g.lng, 2));
	}
	
	//calcDistance method with lat and lng.
	public double calcDistance(double lat, double lng) {
		// Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lng1 - lng2, 2))
		return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
	}
	
}
