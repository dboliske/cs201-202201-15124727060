# Lab 7

## Total

20/20

## Break Down

- Exercise 1 4/4
- Exercise 2 5/5
- Exercise 3 5/5
- Exercise 4 6/6

## Comments

- Your programs are good. However, you need to read questions carefully before you write codes. Just do as the requirements next time. 
