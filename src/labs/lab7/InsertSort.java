// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 29, 2022; 	 name of project: labs.lab7
package labs.lab7;

public class InsertSort<T> extends baseSort<T> {
	// default construct
	public InsertSort() {
		
	}
	
	// non-default construct
	public InsertSort(T[] data) {
		super(data);
	}
	
	// sort method
	@Override
	public void sort() {
		// String data type
		if(super.datalist[0] instanceof String){
			for(int j=1; j < datalist.length; j++) {
				int i = j;
				while(i > 0 && (( (String) datalist[i]).compareTo( (String) datalist[i-1])<0 )) {
					super.swap(i, i-1);
					i--;
				}
			}
		} else if(super.datalist[0] instanceof Double) {
			// Double data type
			for(int j=1; j < datalist.length; j++) {
				int i = j;
				while(i > 0 && ( (double) datalist[i] < (double)datalist[i-1])) {
					super.swap(i, i-1);
					i--;
				}
			}
		} else if(super.datalist[0] instanceof Integer) {
			// Integer data type
			for(int j=1; j < datalist.length; j++) {
				int i = j;
				while(i > 0 && ( (int) datalist[i] < (int)datalist[i-1])) {
					super.swap(i, i-1);
					i--;
				}
			}
		} else {
			System.out.println("Sorry, unsupported data type.");
		}
	}
	
	// run and show metohd
	@Override
	public void RunAndShow() {
		System.out.println("	Insert Sorting	");
		System.out.println("Before sorting: ");
		super.showList();
		sort();
		System.out.println("After sorting: ");
		super.showList();
		System.out.println();
	}

}
