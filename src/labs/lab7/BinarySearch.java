// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 29, 2022; 	 name of project: labs.lab7
package labs.lab7;

import java.util.Scanner;

public class BinarySearch<T> extends baseSearch<T> {
	// default construct
	public BinarySearch() {
		super();
	}
	// non-default construct
	public BinarySearch(T[] datalist) {
		super(datalist);
	}
	// search method
	@Override
	public int search(String key){
		int start = 0;
		int index = -1;
		int end = datalist.length;
		boolean found = false;
		
		while(!found && start!= end) {
			int mid = (start + end)/2;
//				System.out.println(mid);
			if (key.equalsIgnoreCase(datalist[mid].toString())) {
				found = true;
				index = mid;
			} else if(((String)key).compareToIgnoreCase(datalist[mid].toString()) > 0) {
				start = mid + 1;
			} else {
				end = mid;
			}
		}
		return index;
	
	}
	// show method
	@Override
	public void show() {
		System.out.println("	binary Search	");
		System.out.println("item's key: ");
		Scanner input = new Scanner(System.in);
		int index = search(input.nextLine());
		if (index == -1) {
			System.out.println("not found!");
		}else {
			System.out.println("it's index is " + index + " .");
		}
		
	}
}
