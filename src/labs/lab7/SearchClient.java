// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 29, 2022; 	 name of project: labs.lab7
package labs.lab7;

public class SearchClient {
	// use binary search
	public static void binarySearch(Object[] datalist) {
		BinarySearch data = new BinarySearch(datalist);
		data.show();
	}

	public static void main(String[] args) {
		String[] Stringlist = {"c", "html", "java", "python", "ruby", "scala"};
		binarySearch(Stringlist);
	}

}
