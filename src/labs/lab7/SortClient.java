// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 29, 2022; 	 name of project: labs.lab7
package labs.lab7;

public class SortClient {
	// use bubble sort
	public static void bubbleSort(Object[] datalist) {
		BubbleSort data = new BubbleSort(datalist);
		data.RunAndShow();
	}
	// use insert sort
	public static void insertSort(Object[] datalist) {
		InsertSort data = new InsertSort(datalist);
		data.RunAndShow();
	}
	// use select sort
	public static void selectSort(Object[] datalist) {
		SelectSort data = new SelectSort(datalist);
		data.RunAndShow();
	}
	
	public static Object[] convertIntdata(int[] datalist) {
		Object[] data = new Object[datalist.length];
		for(int i=0; i<datalist.length; i++) {
			data[i] = datalist[i];
		}
		return data;
	}
	
	public static Object[] convertDoubledata(double[] datalist) {
		Object[] data = new Object[datalist.length];
		for(int i=0; i<datalist.length; i++) {
			data[i] = datalist[i];
		}
		return data;
	}
	
	public static void main(String[] args) {
		int[] intlist = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		String[] Stringlist = {"cat", "fat", "dog", "apple", "bat", "egg"};
		double[] doublelist = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		Object[] ilist = convertIntdata(intlist);
		Object[] dlist = convertDoubledata(doublelist);
		
		bubbleSort(ilist);
		// bubbleSort(Stringlist);
		// bubbleSort(dlist);
		
		// insertSort(ilist);
		insertSort(Stringlist);
		// insertSort(dlist);
		
		
		// selectSort(ilist);
		// selectSort(Stringlist);
		selectSort(dlist);
	}
}
