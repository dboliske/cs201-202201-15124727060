// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 29, 2022; 	 name of project: labs.lab7
package labs.lab7;

public class baseSort<T> {
	
	protected T[] datalist;
	protected int length;
	
	// default construct
	public baseSort() {
		datalist = null;
		length = -1;
	}
	
	// non-default construct
	public baseSort(T[] datalist) {
		this.datalist = datalist;
		length = datalist.length;
	}
	
	// sort method
	public void sort() {
		
	}
	
	// swap method
	public void swap(int x, int y) {
		T temp = datalist[x];
		datalist[x]=  datalist[y];
		datalist[y] = temp;
	}
	
	// show data method
	public void showList() {
		for(int i=0; i<datalist.length; i++) {
			System.out.print(datalist[i] + " ");
		}
		System.out.println();
	}
	// run and show method
	public void RunAndShow() {
		showList();
	}
	
}
