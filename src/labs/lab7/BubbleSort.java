// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 29, 2022; 	 name of project: labs.lab7
package labs.lab7;

public class BubbleSort<T> extends baseSort<T> {
	// default construct
	public BubbleSort() {
		super();
	}
	
	// non-default construct
	public BubbleSort(T[] data) {
		super(data);
	}
	
	// sort method // support Integer, double, String data type.
	@Override
	public void sort() {
		if(super.datalist[0] instanceof String) {
			boolean flag = false;
			do {
				flag = false;
				for(int i=0; i<super.datalist.length - 1; i++) {
//					System.out.print(datalist[i] + "  " + datalist[i+1] + "   ");
					if (((String) super.datalist[i]).compareTo((String)super.datalist[i+1]) > 0){
						super.swap(i, i+1);
						flag = true;
					}
				}	
			}while(flag);
		}else if(super.datalist[0] instanceof Double) {
			boolean flag = false;
			do {
				flag = false;
				for(int i=0; i<super.datalist.length - 1; i++) {
					if (((double) super.datalist[i]) > ((double)super.datalist[i+1])){
						super.swap(i, i+1);
						flag = true;
					}
				}	
			}while(flag);
		}else if (super.datalist[0] instanceof Integer) {
			boolean flag = false;
			do {
				flag = false;
				for(int i=0; i<super.datalist.length - 1; i++) {
					if (((int)super.datalist[i]) > ((int)super.datalist[i+1])){
						super.swap(i, i+1);
						flag = true;
					}
				}	
			}while(flag);
		}else {
			System.out.println("Sorry, unsupported data type.");
		}
	}	
	
	// run and show method
	@Override
	public void RunAndShow() {
		System.out.println("	Bubble Sorting	");
		System.out.println("Before sorting: ");
		super.showList();
		sort();
		System.out.println("After sorting: ");
		super.showList();
		System.out.println();
	}
}
