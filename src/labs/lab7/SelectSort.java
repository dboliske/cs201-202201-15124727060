// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 29, 2022; 	 name of project: labs.lab7
package labs.lab7;

public class SelectSort<T> extends baseSort<T> {
	// default construct
	public SelectSort(){
		
	}
	// non-default construct
	public SelectSort(T[] data){
			super(data);
	}
	
	@Override
	public void sort() {
		if (super.datalist[0] instanceof String) {
			// String data type
			for(int j=0; j<datalist.length-1; j++) {
				String minstr = (String)datalist[j];
				int index = j;
				for(int i=j+1; i< datalist.length; i++) {
					if (minstr.compareTo((String)datalist[i])>0) {
						minstr = (String)datalist[i];
						index = i;
					}
				}
				if (index != j) {
					super.swap(index, j);
				}
			}
		}else if(super.datalist[0] instanceof Double) {
			// Double data type
			for(int j=0; j<datalist.length-1; j++) {
				double min = (double)datalist[j];
				int index = j;
				for(int i=j+1; i< datalist.length; i++) {
					if (min > (double)datalist[i]) {
						min = (double)datalist[i];
						index = i;
					}
				}
				if (index != j) {
					super.swap(index, j);
				}
			}
		}else if(super.datalist[0] instanceof Integer) {
			// Double data type
			for(int j=0; j<datalist.length-1; j++) {
				double min = (int)datalist[j];
				int index = j;
				for(int i=j+1; i< datalist.length; i++) {
					if (min > (int)datalist[i]) {
						min = (int)datalist[i];
						index = i;
					}
				}
				if (index != j) {
					super.swap(index, j);
				}
			}
		}else {
			System.out.println("Sorry, unsupported data type.");
		}
	}
	
	// run and show metohd
	@Override
	public void RunAndShow() {
		System.out.println("	Select Sorting	");
		System.out.println("Before sorting: ");
		super.showList();
		sort();
		System.out.println("After sorting: ");
		super.showList();
		System.out.println();
	}

}
