// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Feb. 13, 2022; 	 name of project: labs.lab4
package labs.lab4;

/*
 1.Create three instance variables, countryCode, areaCode and number, all of which should be Strings.
2.Write the default constructor.
3.Write the non-default constructor.
4.Write 3 accessor methods, one for each instance variable.
5.Write 3 mutator methods, one for each instance variable.
6.Write a method that will return the entire phone number as a single string (the toString method).
7.Write a method that will return true if the areaCode is 3 characters long.
8.Write a method that will return true if the number is 7 characters long.
9.Write a method that will compare this instance to another PhoneNumber (the equals method).
 * */


public class PhoneNumber {
	// instance variables
	String countryCode;
	String areaCode;
	String number;
	
	// default constructor
	public PhoneNumber()
	{
		countryCode = "+86";
		areaCode = "010";
		number = "1234567";
	}
	// non-default constructor
	public PhoneNumber(String c, String a, String n)
	{
		setCountryCode(c);
		setAreaCode(a);
		setNumber(n);
	}
	// accessor method for countryCode
	public String getCountryCode()
	{
		return countryCode;
	}
	// accessor method for areaCode
	public String getAreaCode()
	{
		return areaCode;
	}
	// accessor method for number
	public String getNumber()
	{
		return number;
	}
	// mutator method for countryCode
	public void setCountryCode(String countryCode)
	{
		this.countryCode = countryCode;
	}
	// mutator method for areaCode
	public void setAreaCode(String areaCode)
	{
		
		this.areaCode = areaCode;
	
	}
	// mutator method for number
	public void setNumber(String number)
	{
		
		this.number = number;
		
	}
	// toString method
	public String toString()
	{
		return countryCode + "-" + areaCode + " " + number;
	}
	// vaild areaCode
	public boolean vaildAreaCode()
	{
		// if the areaCode is 3 characters long
		if (areaCode.length() == 3)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	// vaild number 
	public boolean vaildNumber()
	{
		// if the number is 7 characters long
		if (number.length() == 7)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	// equal method
	public boolean equals(Object obj)
	{
		if(!(obj instanceof PhoneNumber)) {
			return false;
		}
		PhoneNumber p = (PhoneNumber)obj;
		
		
		
//		if (this.toString() == p.toString())
//		{
//			return true;
//		}
//		else {
//			return false;
//		}
		if (this.countryCode != p.countryCode)
		{
			return false;
		}
		else if (this.areaCode != p.areaCode)
		{
			return false;
		}
		else if (this.number != p.number)
		{
			return false;
		}
		else 
		{
			return true;
		}
	}	
}
