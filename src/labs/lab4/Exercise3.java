// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Feb. 13, 2022; 	 name of project: labs.lab4
package labs.lab4;


//9.Now write an application class that instantiates two instances of Potion. 
//One instance should use the default constructor and the other should use the non-default constructor. 
//Display the values of each object by calling the toString method.



public class Exercise3 {

	public static void main(String[] args) {
		Potion p = new Potion(); // default:  name->hypertonic glucose solution ; strength->0.0
		System.out.println(p.toString()); // p --> name: hypertonic glucose solution; strength: 0.0
		
		Potion p2 = new Potion("stimulant", 5.2); 
		System.out.println("name is " + p2.getName());
		System.out.println("strength is " + p2.getStength());
		System.out.println(p2.toString()); // p2 -->  name: stimulant; strength: 5.2
		
		System.out.println(p.equals(p2)); // false
		
		// set and valid
		p.setName("stimulant");
		System.out.println(p.toString());
		p.setStrength(12.5);
		//  p --> name: stimulant; strength: 0.0   p2 -->  name: stimulant; strength: 5.2
		System.out.println(p.vaildStrength(p.getStength()));// false
		System.out.println(p.toString());
		p.setStrength(5.2);
	//  p --> name: stimulant; strength: 5.2   p2 -->  name: stimulant; strength: 5.2
		System.out.println(p.vaildStrength(p.getStength()));//true
		System.out.println(p.toString());
		
		System.out.println(p.equals(p2)); // true
		
		

	}

}
