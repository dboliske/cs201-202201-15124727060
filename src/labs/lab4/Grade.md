# Lab 4

## Total

26/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        6/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        7/8
  * Application Class   1/1
* Documentation         3/3

## Comments

Good, but your GeoLocation and Potion classes don't validate in the mutator methods and PhoneNumber doesn't validate in the mutator methods and the validation methods do not follow the UML diagram.
