// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Feb. 13, 2022; 	 name of project: labs.lab4
package labs.lab4;

//10.Now write an application class that instantiates two instances of PhoneNumber. 
//One instance should use the default constructor and the other should use the non-default constructor.
//Display the values of each object by calling the toString method.


public class Exercise2 {

	public static void main(String[] args) {
		// default constructor
		PhoneNumber p = new PhoneNumber();
		System.out.println(p.getCountryCode() + "-" + p.getAreaCode() + " " + p.getNumber());
		System.out.println(p.toString()); // +86-010 1234567
		// non-default constructor
		PhoneNumber p2 = new PhoneNumber("+1", "773", "7654321");
		System.out.println(p2.toString()); // +1-773 7654321
		
		System.out.println("p1: " + p.toString() + " p2: " + p2.toString());
		System.out.println(p.equals(p2)); //false
		
		System.out.println(p.toString());
		p.setCountryCode("+1");
		System.out.println(p.toString());
		p.setAreaCode("1234");
		System.out.println(p.vaildAreaCode()); // false
		System.out.println(p.toString());
		p.setAreaCode("773");
		System.out.println(p.vaildAreaCode()); // true
		System.out.println(p.toString());
		
		p.setNumber("12345678");
		System.out.println(p.vaildNumber());// false
		System.out.println(p.toString());
		p.setNumber("7654321");
		System.out.println(p.vaildNumber());// true
		System.out.println(p.toString());
		
		System.out.println("p1: " + p.toString() + " p2: " + p2.toString());
		System.out.println(p.equals(p2)); //true
			
	}

}
