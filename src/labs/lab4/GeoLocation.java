// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Feb. 13, 2022; 	 name of project: labs.lab4

package labs.lab4;

/*
 1.Create two instance variables, lat and lng, both of which should be doubles.
2.Write the default constructor.
3.Write the non-default constructor.
4.Write 2 accessor methods, one for each instance variable.
5.Write 2 mutator methods, one for each instance variable.
6.Write a method that will return the location in the format "(lat, lng)" (the toString method).
7.Write a method that will return true if the latitude is between -90 and +90.
8.Write a method that will return true if the longitude is between -180 and +180.
9.Write a method that will compare this instance to another GeoLocation (the equals method).
 * */


public class GeoLocation {
	//instance variables
	private double lat;
	private double lng;
	
	//default construct
	public GeoLocation()
	{
		lat = 0.0;
		lng = 0.0;
	};
	//non-default construct
	public GeoLocation(double lat, double lng)
	{
		setLat(lat);
		setLng(lng);
	}
	// accessor method for lat
	public double getLat()
	{
		return lat;
	}
	// accessor method for lng
	public double getLng()
	{
		return lng;
	}
	// mutator method for lat
	public void setLat(double lat)
	{
		this.lat = lat;
	}
	// mutator method for lng
	public void setLng(double lng)
	{
		this.lng = lng;
	}
	// toString method
	public String toString() 
	{
		// return " lat: " + lat + " lng: " + lng;
		return "(%s , %s)".formatted(lat, lng);
	}
	//validlat method
	public boolean validLat(double lat)
	{
		// if the latitude is between -90 and +90 , return true
		if (lat >= -90 && lat <= 90)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//validlng method
	public boolean validLng(double lng)
	{
		// if the longitude is between -180 and +180 , return true
		if (lng >= -180 && lng <= 180)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	// equal method
	public boolean equals(Object obj)
	{
		if(!(obj instanceof GeoLocation)) {
			return false;
		}

		GeoLocation g = (GeoLocation) obj;
		//  compare this instance to another GeoLocation
		if (this.lat != g.lat)
		{
			return false;
		}
		else if (this.lng != g.lng)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
}
