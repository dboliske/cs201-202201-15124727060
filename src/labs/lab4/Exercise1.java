// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Feb. 13, 2022; 	 name of project: labs.lab4

package labs.lab4;

// 10.Now write an application class that instantiates two instances of GeoLocation.
// One instance should use the default constructor and the other should use the non-default constructor. 
// Display the values of the instance variables by calling the accessor methods.

public class Exercise1 {

	public static void main(String[] args) {
		// default constructor
		GeoLocation g = new GeoLocation(); 
		// non-default constructor
		GeoLocation g2 = new GeoLocation(22.5, -1.2);
		
		System.out.println(g.toString()); // g (0.0, 0.0)
		g.setLat(22.5);
		System.out.println(g.toString());
		g.setLng(-1.2);
		// g --> (22.5, -1.2)  g2 --> (22.5, -1.2)
		System.out.println(g.toString());
		System.out.println(g2.toString());
		System.out.println(g.equals(g2));//true
		g2.setLat(0);
		// g --> (22.5, -1.2)  g2 --> (0, -1.2)
		System.out.println(g.equals(g2));//false
		g2.setLat(22.5);
		// g --> (22.5, -1.2)  g2 --> (22.5, -1.2)
		System.out.println(g.equals(g2));//true
		
		
	}

}
