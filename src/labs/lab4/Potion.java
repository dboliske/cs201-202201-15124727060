// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Feb. 13, 2022; 	 name of project: labs.lab4

package labs.lab4;

/*
 1.Create two instance variables, name (a String) and strength (a double).
2.Write the default constructor.
3.Write the non-default constructor.
4.Write two accessor methods, one for each instance variable.
5.Write two mutator methods, one for each instance variable.
6.Write a method that will return the entire as a single string (the toString method).
7.Write a method that will return true if the strength is between 0 and 10.
8.Write a method that will compare this instance to another Potion (the equals method).
 * */


public class Potion {
	// instance variables
	private String name;
	private double strength;
	
	// default constructor
	public Potion()
	{
		name = "hypertonic glucose solution";
		strength = 0.0d;
	}
	// non-default constructor
	public Potion(String name, double strength)
	{
		setName(name);
		setStrength(strength);
	}
	// accessor method for name
	public String getName()
	{
		return name;
	}
	// accessor method for strength
	public double getStength()
	{
		return strength;
	}
	// mutator method for name
	public void setName(String name)
	{
		this.name = name;
	}
	// mutator method for strength
	public void setStrength(double strength)
	{
		// if the strength is between 0 and 10.	
		this.strength = strength;
		
	}
	//toString method
	public String toString()
	{
		return "name: " + name + "; strength: " + strength;
	}
	//vaild strength
	public boolean vaildStrength(double strength)
	{
		if (strength >= 0 && strength <= 10)
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}
	// equals method
	public boolean equals(Object obj)
	{
		if(!(obj instanceof Potion)) {
			return false;
		}
		
		Potion p = (Potion) obj;
	
		if (this.name != p.name)
		{
			return false;
		}
		else if(this.strength != strength)
		{
			return false;
		}
		else 
		{
			return true;
		}
	}
	
}
