// name: ShaoQiang Guo ;	course: CS201;	sec: 06
// date: Jan. 31, 2022; 	name of project: labs.lab1
package labs.lab1;
import java.util.Scanner;
import java.io.IOException;
import java.io.File;


public class Exercise6 {

	public static void main(String[] args) throws IOException {
//		17.Write a program that will convert inches to centimeters:
		Scanner input = new Scanner(System.in);
//		18.Prompt the user for inches.
		System.out.println("Please input a number in inches: ");
//		19.Convert inches to centimeters
		// 1 inch �� 2.54 cm
		double inch_value = Double.parseDouble(input.nextLine());
		double centimeter_value = inch_value * 2.54;
		input.close();
		System.out.println("the value you input is " + centimeter_value + " cm .");
//		20.Display the result to the console.
//		21.Test your program with various inputs. Use a test table like above with as many rows as needed to test your program.
//		File = new File("src/labs.lab1/inch_data.txt");
		// sorry, relative path is not work.
		File inch_data = new File("C:/Users/Guo/git/cs201-202201-15124727060/src/labs/lab1/inch_data.txt");
		Scanner lines = new Scanner(inch_data);
		System.out.println("\n load file...");
		while(lines.hasNextLine())
		{
			String line = lines.nextLine();
			System.out.print(line + " inches in equal to ");
			double value = Double.parseDouble(line);
			// 1 inch �� 2.54 cm
			System.out.println("%.2f centimeters .".formatted(value * 2.54));
		}
		
		System.out.println("Goodbye !");
		
	}

}
