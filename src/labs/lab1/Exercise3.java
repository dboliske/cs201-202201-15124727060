// name: ShaoQiang Guo ;	course: CS201;	sec: 03
// date: Jan. 27, 2022; 	name of project: labs.lab1
package labs.lab1; 

import java.util.Scanner;

public class Exercise3 {

	public static void main(String[] args) {
//		Getting a char from keyboard input:Prompt a user for a first name; 
//		display the user's first initial to the screen. (Hint: Use the String method charAt(0)).
		System.out.println("Please input your first name: ");
		Scanner input = new Scanner(System.in);
		char f = input.nextLine().charAt(0);
		// print user's first inital
		System.out.println("the first char of your first name is " + f);		
		input.close();
	}

}
