// name: ShaoQiang Guo ;	course: CS201;	sec: 04
// date: Jan. 27, 2022; 	name of project: labs.lab1
package labs.lab1;

import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class Exerclse4 {

	public static void main(String[] args) throws IOException {
//		8.Write a program that will do the following:
//		9.Prompt the user for a temperature in Fahrenheit, convert the Fahrenheit to Celsius and display the result.
//		10.Prompt the user for a temperature in Celsius, convert the Celsius to Fahrenheit and display the result.
//		11.Test your program with various temperatures: low, high, middle. Use a test table with as many rows as needed to test your program. 
//		Are you satisfied that your program works as expected? Submit your test plan and its results.
		// Fahrenheit = 32��F + Celsius * 1.8
		System.out.println("Please input a temperature in Fahrenheit: ");
		Scanner input = new Scanner(System.in);
		double Fahr = Double.parseDouble(input.nextLine());
		double Cels = (Fahr - 32) / 1.8;
		//print the convert result.
		System.out.println("the temperature you input in Celsius is %.2f".formatted(Cels) + " ��.");
		
		System.out.println("Please input a temperature in Celsius: ");
		double Cels2 = Double.parseDouble(input.nextLine());
		double Fahr2 = Cels2 * 1.8 + 32;
		//print the convert result.
		System.out.println("the temperature you input in Fahrenheit is %.2f".formatted(Fahr2) + " ��F.");
		input.close();

//		File mydata = new File("src/labs.lab1/weatherdata.csv");
		// sorry, relative path is not work.
		File mydata = new File("C://Users/GuoShaoQiang/git/cs201-202201-15124727060/src/labs/lab1/weatherdata.csv");
		Scanner csvline = new Scanner(mydata);
		// I define temputure range: low(<10), high(>30), middle(10~30)
		while(csvline.hasNextLine())
		{
			String line = csvline.nextLine();
			String temptures = line.substring(line.indexOf(",") + 1);
			System.out.print("the tempture is :" + temptures + " ��");
			double tempture = Double.parseDouble(temptures);
			if (tempture < 10)
			{
				System.out.println("; it's a low tempture.");
			}
			else if (tempture >= 10 && tempture <= 30)
			{
				System.out.println("; it's a middle tempture.");
			}
			else
			{
				System.out.println("; I think it's a high tempture.");
			}
		}
	}

}
