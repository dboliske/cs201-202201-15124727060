// name: ShaoQiang Guo ;	course: CS201;	sec: 02
// date: Jan. 27, 2022; 	name of project: labs.lab1
package labs.lab1;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
//		Performing arithmetic calculations:Output the result of the following calculations; be sure to write a descriptive comment for each output so the reader knows what is being calculated:
//		3.Your age subtracted from your father's age
//		4.Your birth year multiplied by 2
//		5.Convert your height in inches to cms
//		6.Convert your height in inches to feet and inches where inches is an integer (mode operator)
		System.out.println("Please input your age: ");
		Scanner input = new Scanner(System.in);
		int kid_age = Integer.parseInt(input.nextLine());
		System.out.println("Please input your father's age: ");
		int dad_age = Integer.parseInt(input.nextLine());
		// print father's age subtract user's age
		System.out.println("Your father's age subtract Your age is " + (dad_age - kid_age));
		
		System.out.println("Please input your birth year: ");
		int year = Integer.parseInt(input.nextLine());
		// birth year multiplied by 2
		System.out.println("your birth year multiplied by 2 is " + (year * 2));
		
		System.out.println("Please input your height(inch): ");
		float height = Float.parseFloat(input.nextLine());
		// convert height from inches to cms 
		// 1 inch �� 2.54 cm
		System.out.println("your height is %.2f".formatted(height * 2.54) + "cm");
		
		// 1 foot = 12 inch
		// Convert height from inch to feet and inches
		System.out.println("your height is %d".formatted((int)height / 12) + " feet and %.2f".formatted(height % 12) + " inches.");
		input.close();
	}

}
