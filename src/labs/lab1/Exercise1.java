// name: ShaoQiang Guo ;	course: CS201;	sec: 01
// date: Jan. 27, 2022; 	name of project: labs.lab1

package labs.lab1;
import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		// Reading input and writing output:Writing a Java program that will prompt a user for a name; 
		// save the input and echo the name to the console.
		System.out.println("Please input your name: ");
		// get user's name
		Scanner name = new Scanner(System.in);
		// print user's name
		System.out.println("Echo: " + name.nextLine());
		name.close();
	}

}
