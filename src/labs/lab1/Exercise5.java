// name: ShaoQiang Guo ;	course: CS201;	sec: 05
// date: Jan. 31, 2022; 	name of project: labs.lab1
package labs.lab1;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class Exercise5 {

	public static void main(String[] args) throws IOException {
//		12.Write a program that will do the following:
		Scanner input = new Scanner(System.in);
//		13.Prompt the user for the length, width, and depth in inches of a box.
		System.out.println("Please input the length in inches of a box: ");
		double length = Double.parseDouble(input.nextLine());
		System.out.println("Please input the width in inches of a box: ");
		double width = Double.parseDouble(input.nextLine());
		System.out.println("Please input the depth in inches of a box: ");
		double depth = Double.parseDouble(input.nextLine());
//		14.Calculate the amount of wood (square feet) needed to make the box.
		// amount = (length * width + length * depth + width * depth) * 2
		double amount = (length * width + length * depth + width * depth) * 2;		
//		15.Display the result to the screen with a descriptive message.
		System.out.println("The amount of wood need is " + amount + " square feet.");
//		16.Test your program with various inputs. Use a test table like above with as many rows as needed to test your program.
			
//		File mydata = new File("src/labs.lab1/boxes.txt");
		// sorry, relative path is not work.
		File mydata = new File("C:/Users/Guo/git/cs201-202201-15124727060/src/labs/lab1/boxes.txt");
		System.out.println("reading file...");
		Scanner txtline = new Scanner(mydata);
		while(txtline.hasNextLine())
		{
			String line = txtline.nextLine();
			// find the length, width and depth of each box
			String line_part1 = line.substring(line.indexOf(',') + 1);
			String line_part2 = line_part1.substring(line_part1.indexOf(',') + 1);
			String line_part3 = line_part2.substring(line_part2.indexOf(',') + 1);
			// System.out.println("Box length: " + line_part1 + " width: " + line_part2 + " depth: " + line_part3);
			String boxlength = line_part1.substring(0, line_part1.indexOf(','));
			String boxwidth = line_part2.substring(0, line_part2.indexOf(',') );
			String boxdepth = line_part3.substring(line_part3.indexOf(',') + 1);
			System.out.println("Box length: " + boxlength + " width: " + boxwidth + " depth: " + boxdepth);
			// convert string type to double
			double length_value = Double.parseDouble(boxlength);
			double width_value = Double.parseDouble(boxwidth);
			double depth_value = Double.parseDouble(boxdepth);
			// compute the amount of wood (square feet) needed to make the box
			double amount_value = (length_value * width_value + length_value * depth_value + width_value * depth_value) * 2;
			System.out.println("need " + amount_value + " square feet wood.");			
		}
		System.out.print("GoodBye.");
		input.close();
	}

}
