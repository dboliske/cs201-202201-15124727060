// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: Mar. 11, 2022; 	 name of project: labs.lab6
package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class deliApplication {
	
	// Add customer to queue
	public static ArrayList<String> addCustomer(ArrayList<String> customers, Scanner input){
		System.out.println("Please input the customer's name: ");
		customers.add(input.nextLine());
		return customers;
	}
	
	// help customer function
	public static String helpCustomer(ArrayList<String> customers, int index){
		if (customers.size() > 0)
		{
			System.out.println("help customer " + customers.get(index) + " and remove from customers queue.");
			return customers.remove(index);
		}else { // no nember in queue, return none
			return "";
		}	
	}
	
	// show customers queue
	public static void showCustomers(ArrayList<String> customers) {
		if (customers.size() <= 0) {
			System.out.println("Sorry, nobody in customers queue.");
		}
		else if (customers.size() == 1) {
			System.out.println("Only one member in customers queue, and he/she is " + customers.get(0));
		}else {
			System.out.println("There are " + customers.size() + " customers in quene, they are: ");
			for(String name : customers) {
				System.out.println(name);
			}
			System.out.println();
		}
		// customers.trimToSize();
	}
	
	public static void main(String[] args) {	
		boolean flag = false;
		Scanner input = new Scanner(System.in);
		ArrayList<String> customers = new ArrayList<String>();
		String[] options = {"Add customer to queue", "Help customer", "Exit"};
		
		do {
			for (int i=0; i<options.length; i++){
				System.out.println((i+1) + ". " + options[i]);
			}
			System.out.print("Choice: ");
			switch (input.nextLine()){
				// Add customer to queue
				case "1":
					customers = addCustomer(customers, input);
					showCustomers(customers);
					break;
				// help customer function
				case "2":
					System.out.println(helpCustomer(customers, 0));
					showCustomers(customers);
					break;
				// exit
				case "3":
					flag = true;
					break;
				default:
					System.out.println("Sorry, it's an unspported function.");				
				}
		
		}while(!flag);
		System.out.println("Goodbye!");

	}
}