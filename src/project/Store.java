// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: April. 25, 2022; 	 name of project: labs.project
package project;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Store {
	private ArrayList<Commodity> stores;
	private ArrayList<Commodity> saledStores;
	private boolean sale;
	private boolean init;
	
	// default constructor
	public Store(){
		stores = new ArrayList<Commodity>();
		saledStores = new ArrayList<Commodity>();
		sale = false;
		init = false;
	}
	

	// non-default constructor with filename
	public Store(String filename){	
		stores = new  ArrayList<Commodity>();
		saledStores = new ArrayList<Commodity>();
		sale = false;
		try {
			File f = new File(filename);
			Scanner warehouse = new Scanner(f);
			int linenum = 0;
			while(warehouse.hasNextLine()) {
				String line = warehouse.nextLine();
				String[] linelist = line.split(",");
				String name = linelist[0];
				double price = Double.parseDouble(linelist[1]);
				linenum += 1;
				if(linelist.length == 2) {
					ShelvedCommodity sc = new ShelvedCommodity(name, price);
					stores.add(sc);
				}else if(linelist.length == 3) {
					if(Time.isTimeStr(linelist[2])) {
						ProduceCommodity pc = new ProduceCommodity(name, price, linelist[2]);
						stores.add(pc);
					}else {
						int age = Integer.parseInt(linelist[2]);
						AgeRestrictedCommodity arc = new AgeRestrictedCommodity(name, price, age);
						
						stores.add(arc);
					}
				}else {
					System.out.println("Sorry, wrong data-format, please check your file, the " + linenum + "line .");
				}	
			}
		}catch (FileNotFoundException e) {
			System.out.println("Sorry, File not found, please check your filepath!");
		}catch (IOException e) {
			System.out.println("Error , Cannot read file. ");
		}catch (Exception e) {
			System.out.println("Sorry, Something wrong when parse data. ");
			e.printStackTrace();
		}
		init = true;
	}
	
	//accessor method for saledStores
	public ArrayList<Commodity> getSaledStores() {
		return saledStores;
	}
	
	//mutator method for saledStores
	public void setSaledStores(ArrayList<Commodity> saledStores) {
		this.saledStores = saledStores;
		setSaled(true);
	}
	

	// acessor method for store.size()
	public int getStoreSize() {
		if(!init) {
			return -1;
		}
		return stores.size();	
	}

	// acessor method for store
	public ArrayList<Commodity> getStores() {
		return stores;
	}

	// mutator method for store.size()
	public void setStores(ArrayList<Commodity> stores) {
		this.stores = stores;
	}
	

	// acessor method for sale
	public boolean isSaled() {
		return sale;
	}

	// mutator method for sale
	public void setSaled(boolean sale) {
		this.sale = sale;
	}

	// acessor method for init
	public boolean isInit() {
		return init;
	}

	// mutator method for sale
	public void setInit(boolean init) {
		this.init = init;
	}
	

	//compute price method
	public double computePrice() {
		if(init) {
			double totalPrice = 0d;
			for(int i = 0; i < stores.size(); i++) {
				totalPrice += stores.get(i).getPrice();
			}
			return totalPrice;
		}else {
			System.out.print("sorry, your store is not init yet.");
			return -1;
		}		
	}
	

	//compute price method with param commodity list
	public static double computePrice(ArrayList<Commodity> clist) {
		if(clist.size()> 0) {
			double totalPrice = 0d;
			for(int i = 0; i < clist.size(); i++) {
				totalPrice += clist.get(i).getPrice();
			}
			return totalPrice;
		}else {
			System.out.print("sorry, the commodity arraylist is not init yet.");
			return -1;
		}
		
	}
	

	//compute price method with param store
	public static double computePrice(Store s) {
		if(s.isInit()) {
			double totalPrice = 0d;
			for(int i = 0; i < s.stores.size(); i++) {
				totalPrice += s.stores.get(i).getPrice();
			}
			return totalPrice;
		}else {
			System.out.print("sorry, the store is not init yet.");
			return -1;
		}
		
	}
	

	// show commodity message method
	public void show() {
		for(int i = 0; i < stores.size(); i++) {
			System.out.println("" + (i+1) + "  " +  stores.get(i).toString());
		}	
	}
	

	// show commodity message with save-to-file-format method
	public void showFileStream() {
		for(int i = 0; i < stores.size(); i++) {
			System.out.println("" + (i+1) + "  " + stores.get(i).toFilestream());
		}	
	}
	
}
