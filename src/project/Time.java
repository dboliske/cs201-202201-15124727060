// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: April. 25, 2022; 	 name of project: labs.project
package project;

public class Time {
	private int year;
	private int month;
	private int day;
	private boolean init;
	
	// default constructor
	public Time(){
		year = 1970;
		month = 1;
		day = 1;
		init = true;
	}
	
	// non-default constructor
	public Time(int year, int month, int day){
		init = true;
		setYear(year);
		setMonth(month);
		setDay(day);
	}
	
	// accessor method for year
	public int getYear() {
		return year;
	}
	
	// mutator method for year
	public void setYear(int year) {
		if (year > 1970 && year < 9999)
		{
			this.year = year;
		}else {
			this.year = 1970;
			init = false;
		}
	}
	
	// accessor method for month
	public int getMonth() {
		return month;
	}
	
	// mutator method for year
	public void setMonth(int month) {
		if (month > 0 && month < 13)
		{
			this.month = month;
		}else {
			this.month = 1;
			init = false;
		}
	}
	
	// accessor method for day
	public int getDay() {
		return day;
	}
	
	// mutator method for day
	public void setDay(int day) {
		if (day > 0 && day < 32)
		{
			this.day = day;
		}else {
			this.day = 1;
			init = false;
		}
	}

	// set init failed
	public void initFailed() {
		init = false;
	}
	
	// vaild if time init success
	public boolean isInit() {
		return init;
	}
	
	// vaild year method
	public boolean vaildYear()
	{
		if (year >= 1970 && year <= 9999)
		{
			return true;
		}
		else {
			return false;
		}
	}

	// vaild month method
	public boolean vaildMonth()
	{
		if (month >= 1 && month <= 12)
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	// vaild day method
	public boolean vaildDay()
	{
		if (! this.vaildMonth())
		{
			System.out.println("Not a suitable month.");
			return false;
		}
		if (month == 4 || month == 6 || month == 9 || month == 11)
		{
			if (day >= 1 && day <= 30)
			{
				return true;
			}
			else {
				return false;
			}
		}
		else if (month == 2)
		{
			if (year %4 == 0 && day >=1 && day <= 29)
			{
				return true;
			}
			else if (day >=1 && day <= 28)
			{
				return true;
			}
			else {
				return false;
			}
		}
		else { // 1 3 5 7 8 10 12
			if (day >= 1 && day <= 31)
			{
				return true;
			}
			else {
				return false;
			}
		}		
	}

	// vaild time method
	public boolean vaildTime(Object obj)
	{
		if(!(obj instanceof Time)) {
			return false;
		}
		Time t = (Time)obj;
		if(!(t.vaildYear())) {
			return false;
		}else if(!(t.vaildMonth())) {
			return false;
		}else if(!(t.vaildDay())) {
			return false;
		}else {
			return true;
		}
	}
	
	// toString method format: day/month/year
	public String toString() {
		if (!init) {
			return "Sorry, wrong time format, please init it again.";
		}
		String daystr = day > 10 ? ""+day : "0"+day;
		String monthstr = month > 10 ? ""+month : "0"+month;
		return monthstr + "/" + daystr + "/" + year;
	}
	
	// toString method format: year-month-day
	public String toStringBlank() {
		if (!init) {
			return "Sorry, wrong time format, please init it again.";
		}
		String daystr = day > 10 ? ""+day : "0"+day;
		String monthstr = month > 10 ? ""+month : "0"+month;
		return year + "-" + monthstr + "-" + daystr;
	}
	
	// equal method
	public boolean equals(Object obj) {
		if(!(obj instanceof Time)) {
			return false;
		}
		Time t = (Time)obj;
		if(this.year != t.year) {
			return false;
		}else if(this.month != t.month) {
			return false;
		}else if(this.day != t.day) {
			return false;
		}else {
			return true;
		}
	}
	
	// parse timestr method
	public static Time parseTimeStr(String timestr) {
		Time t;
		Time t_failedInit = new Time();
		t_failedInit.initFailed();
		if (timestr.contains("/")) { // eg: 04/28/2022
			String[] timelist = timestr.split("/");
			int year = Integer.parseInt(timelist[2]);
			int month = Integer.parseInt(timelist[0]);
			int day = Integer.parseInt(timelist[1]);
			t = new Time(year, month, day);
			if(t.isInit()) {
				return t;
			}else {
				return t_failedInit;
			}
		}else if(timestr.strip().contains("-")) { // eg: 2022/04/28
			String[] timelist = timestr.split("-");
			int year = Integer.parseInt(timelist[0]);
			int month = Integer.parseInt(timelist[1]);
			int day = Integer.parseInt(timelist[2]);
			t = new Time(year, month, day);
			if(t.isInit()) {
				return t;
			}else {
				return t_failedInit;
			}
		}else {
			return t_failedInit;
		}
	}
	
	// valid timestr method
	public static Boolean isTimeStr(String timestr) {
		Time t;
		if (timestr.contains("/")) { // eg: 04/28/2022
			String[] timelist = timestr.split("/");
			int year = Integer.parseInt(timelist[2]);
			int month = Integer.parseInt(timelist[0]);
			int day = Integer.parseInt(timelist[1]);
			t = new Time(year, month, day);
			if(t.isInit()) {
				return true;
			}else {
				return false;
			}
		}else if(timestr.strip().contains("-")) { // eg: 2022/04/28
			String[] timelist = timestr.split("-");
			int year = Integer.parseInt(timelist[0]);
			int month = Integer.parseInt(timelist[1]);
			int day = Integer.parseInt(timelist[2]);
			t = new Time(year, month, day);
			if(t.isInit()) {
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}

}
