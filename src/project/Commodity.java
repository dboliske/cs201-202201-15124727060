// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: April. 25, 2022; 	 name of project: labs.project
package project;

public class Commodity {
	
	private String name;
	private double price;
	private boolean init;
	

	// default constructor
	public Commodity() {
		name = "pencil";
		price = 1.2;
		init = true;
	}
	
	// non-default constructor
	public Commodity(String name, double price) {
		this.price = 1.2;
		init = true;
		setName(name);
		setPrice(price);
	}
	
	// accessor method for name
	public String getName() {
		return name;
	}
	
	// mutator method for name
	public void setName(String name) {
		this.name = name;
	}
	
	// accessor method for price
	public double getPrice() {
		return price;
	}
	
	// mutator method for price
	public void setPrice(double price) {
		if(price >= 0) {
			this.price = price;
		}else {
			init = false;
		}
	}
	
	// mutator method for init
	public boolean isInit() {
		return init;
	}
	// accessor method for init
	public void setInit(boolean init) {
		this.init = init;
	}
	
	// vaild method for price
	public boolean vaildPrice(double price) {
		if(price <= 0) {
			return false;
		}
		return true;
	}
	
	// toString method
	public String toString() {
		return "It's " + name + " and its price is " + price + " dollar .\n";
	}
	
	// allowsaled method
	public boolean allowSaled() {
		return false;
	}
	
	// equals method
	public boolean equals(Object obj) {
		if(!(obj instanceof Commodity)){
			return false;
		}
		
		Commodity c = (Commodity)obj;
		if(this.name.compareToIgnoreCase(c.name) != 0) {
			return false;
		}else if(Math.abs(this.price - c.price)> 0.01) {
			return false;
		}else {
			return true;
		}
		
	}
	
	// filestream method
	public String toFilestream() {
		return name + "," + price;
	}

}
