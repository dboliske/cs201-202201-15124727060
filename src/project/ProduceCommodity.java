// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: April. 25, 2022; 	 name of project: labs.project
package project;


public class ProduceCommodity extends Commodity {
	private String expirationDate;
	private Time time;
	
	//default constructor
	public ProduceCommodity() {
		super();
		expirationDate = "01/01/1970";
		time = Time.parseTimeStr("01/01/1970");
	}
	
	// non-default constructor
	public ProduceCommodity(String name, double price, String exirationData) {
		super(name, price);
		setExpirationDate(exirationData);
		time = Time.parseTimeStr(exirationData);
		super.setInit(Time.isTimeStr(exirationData));
		
	}
	
	// non-default constructor
	public ProduceCommodity(String name, double price, Time time) {
		super(name, price);
		this.time = time;
		super.setInit(Time.isTimeStr(this.time.toString()));
		setExpirationDate(this.time.toString());
	}
	
	// non-default constructor
	public ProduceCommodity(String name, double price, int year, int month, int day) {
		super(name, price);
		time = new Time(year, month, day);
		super.setInit(Time.isTimeStr(time.toString()));
		setExpirationDate(time.toString());
	}
	
	//accessor method
	public String getExpirationDate() {
		return expirationDate;
	}
	
	// mutator method
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	// mutator method for year
	public void setYear(int year) {
		time.setYear(year);;
	}
	
	// mutator method for month
	public void setMonth(int month) {
		time.setMonth(month);
	}
	// mutator method for Time
	public void setTime(Time time) {
		this.time = time;
	}

	// mutator method for day
	public void setDay(int day) {
		time.setDay(day);
	}

	//accessor method for time
	public Time getTime() {
		return time;
	}

	// vaild method
	public boolean vaildExpirationDate(String expirationDate) {
		return false;
	}
	
	// toString method
	@Override
	public String toString() {
		if (!time.isInit()) {
			return "Sorry, wrong time format, please init it again.";
		}
		return super.toString() + "Attention please, its expiration date is " + time.toString()+" .\n";
	}
	
	// allownSaled
	@Override
	public boolean allowSaled() {
		if(vaildExpirationDate(expirationDate)) {
			return true;
		}
		return false;
	}
	
	// equals method
	@Override
	public boolean equals(Object obj) {
		if (!(super.equals(obj))) {
			return false;
		}else if(!(obj instanceof ProduceCommodity)) {
			return false;
		}
		ProduceCommodity p = (ProduceCommodity) obj;
		if(!(this.time.equals(p.time))) {
			return false;
		}
		return true;
	}
	
	// filestream method
	@Override
	public String toFilestream() {
		return super.toFilestream() + "," + time.toString();
	}
	
}
	
