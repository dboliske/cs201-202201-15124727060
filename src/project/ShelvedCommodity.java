// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: April. 25, 2022; 	 name of project: labs.project
package project;

public class ShelvedCommodity extends Commodity {
	
	// default constructor
	public ShelvedCommodity() {
		super();
	}
	
	// non-default constructpr
	public ShelvedCommodity(String name, double price) {
		super(name, price);
	}
	
	// toString method
	@Override
	public String toString() {
		return "This is a shelved commodity; it's " + super.getName() + 
				 " and it worth " + super.getPrice() + " dollar .\n";
	}
	
	// allownSaled method
	@Override
	public boolean allowSaled() {
		return true;
	}
	
	// equals method
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	// filestream method
	@Override
	public String toFilestream() {
		return super.toFilestream();
	}
	
}
