// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: April. 25, 2022; 	 name of project: labs.project
package project;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class Application {
	
	// readfile function
	public static Store readFile(String filename){
		Store s = new Store(filename);
		return s;
	}
	
	// writeFile function
	public static void writeFile(String filename, Store s){
		File f = new File(filename);
		try {
			if(!f.exists()){
				f.createNewFile();
			}
			FileWriter f_writer = new FileWriter(f);
			for(int i=0; i<s.getStores().size(); i++) {
				System.out.println(s.getStores().get(i).toFilestream());
				f_writer.write(s.getStores().get(i).toFilestream() + "\n");
			}
			f_writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Sorry, something wrong when write data to file...");
		}
		System.out.println("All commodities has been saved to file, well done !");		
	}
	
	// init commodity function
	public static Commodity initCommodity(Scanner input, int type) {
		System.out.println("Commodity name: ");
		String name = input.nextLine();
		boolean flag = false;
		boolean init_price = false;
		double price = 0;
		do {
			try {
				System.out.println("Commodity price: ");
				price = Double.parseDouble(input.nextLine());
				init_price = true;
				flag = true;
			}catch (NumberFormatException e) {
				System.out.println("Please check your input and input price again");
				flag = false;
			}
		}while(!flag);

		if(type == 2 && init_price) {
			boolean flag_age = false;
			do {
				try {
					System.out.println("useable age: ");
					int age = Integer.parseInt(input.nextLine());
					AgeRestrictedCommodity arc = new AgeRestrictedCommodity(name, price, age);
					if (arc.isInit()) {
						return arc;
					}else {
						System.out.println("Commodity add failed, please check your input and input again");
						flag_age = false;
					}
				}catch (NumberFormatException e) {
					System.out.println("Please check your input and input age again");
					flag_age  = false;
				}
			}while(!flag_age);
			
		}else if(type == 3 && init_price){
			boolean flag_date = false;
			do {
				try {
					System.out.println("Commodity expirationDate: (month/day/year, such as 01/04/2022)");
					String date = input.nextLine();
					ProduceCommodity pc = new ProduceCommodity(name, price, date);
					if (pc.isInit()) {
						return pc;
					}else {
						System.out.println("Commodity add failed, please check your input and input date again ");
						flag_date = false;
					}
				}catch (NumberFormatException e) {
					System.out.println("Please check your input and input date again");
					flag_date  = false;
				}
			}while(!flag_date);
		}else{
			ShelvedCommodity sc = new ShelvedCommodity(name, price);
			if (sc.isInit()) {
				return sc;
			}else {
				System.out.println("Commodity add failed, please check your input");
				return null;
			}
		}
		return null;
	}
	
	// add function -> add more same commodies one time 
	public static Store addMore(Store s, Commodity c, Scanner input) {
		boolean flag = false;
		do {
			try {
				System.out.println("==>" + c.getPrice());
				System.out.println("How many commodites do you want to add ? ");
				int number = Integer.parseInt(input.nextLine());
				if(number > 0) {
					for(int i=0; i<number; i++) {
						s.getStores().add(c);
					}
					flag = false;
					return s;
				}else {
					flag = true;
					System.out.println("check your input, try again.");
				}	
			}catch(NumberFormatException e) {
				flag = true;
				System.out.println("Integer please, try again.");
			}
		}while(flag);
		return s;
	}
	
	// add function
	public static Store addCommodity(Store s, Scanner input) {
		boolean flag = false;
		String[] prompts = {"Please select new commodity type :", "1.Shelved Commodity (name, price)",
				"2.AgeRestricted Commodity (name, price, age)", "3. Produce Commodity (name, price, expirationDate)", "Select: "};
		do {
			for(String p: prompts) {
				System.out.println(p);
			}
			switch(input.nextLine()) {
				case "1":
					Commodity c1 = initCommodity(input, 1);
					System.out.println(c1.toString());
					s = addMore(s, c1, input);
					flag = true;
					break;
				case "2":
					Commodity c2 = initCommodity(input, 2);
					System.out.println(c2.toString());
					s = addMore(s, c2, input);
					flag = true;
					break;
				case "3":
					Commodity c3 = initCommodity(input, 3);
					System.out.println(c3.toString());
					s = addMore(s, c3, input);
					flag = true;
					break;
				default:
					System.out.print("Sorry, invalid choice, please check your input");	
					flag = false;
			}	
		}while(!flag);
		System.out.println("add commodity success!");
		return s;
	}
	
	// sale function
	public static Store saleCommodity(Store s, Scanner input) {
		ArrayList<Integer> indexes = findCommodity(s, input);
		if(indexes.size() == 0) {
			System.out.println("Sorry, no commodity found, and nothing to sale");
			return s;
		}else {
			boolean flag = false;			
			do {
				System.out.println("there are " + indexes.size() + " commodities exist, "
						+ "input the number you want to sale: (0 - " + indexes.size() + ")");

				try {
					int number = Integer.parseInt(input.nextLine());
					if(number == 0) {
						System.out.println("nothing to sale");
						return s;
					}else if(number > 0 && number <= indexes.size()) {
						ArrayList<Commodity> salestores = new ArrayList<Commodity>();
						Commodity c = s.getStores().get(indexes.get(0));
						int count = 0;
						for(int index = s.getStoreSize() - 1; index >= 0 && count < number; index--) {
							if (c.equals(s.getStores().get(index))) {
								salestores.add(s.getStores().get(index));
								System.out.println("====>" + s.getStores().get(index).toFilestream());
								s.getStores().remove(index);
								count += 1;
							}			
						};
						s.setSaledStores(salestores);
					}else {
						flag = true;
						System.out.println("no enough commodities to sale, please input again");
					}
				}catch (Exception e) {
					flag = true;
					System.out.println("Please check your input and input again. ");
					e.printStackTrace();
				}
			}while(flag);
			return s;
		}
		
	}
	
	// change function
	public static Store changeCommodity(Store s, Scanner input) {
		ArrayList<Integer> indexes = findCommodity(s, input);
		if(indexes.size() == 0) {
			System.out.println("Sorry, no commodity found, and nothing to change");
			return s;
		}else {
			System.out.println("change name or price ? (input name or price please)");
			boolean flag = false;
			do {
				String choice = input.nextLine();
				if (choice.compareToIgnoreCase("name") == 0) {
					System.out.println("new name:");
					String newName = input.nextLine();
					for(int index: indexes) {
						s.getStores().get(index).setName(newName);
					}
				}else if(choice.compareToIgnoreCase("price") == 0) {
					boolean flag_double = false;;
					
					do {
						System.out.println("new price:");
						try {
							double newPrice = Double.parseDouble(input.nextLine());
							for(int index: indexes) {
								s.getStores().get(index).setPrice(newPrice);
							}
						}catch (Exception e) {
							System.out.println("Please check your input and input again. ");
						}
						
					}while(flag_double);
				}else {
					System.out.println("input name or price, please");
				}
	
			}while(flag);
			return s;
		}
	}
	
	// find fuction 
	public static ArrayList<Integer> findCommodity(Store s, Scanner input) {
		String[] prompts = {"Please select commodity type :", "1.Shelved Commodity (name, price)",
				"2.AgeRestricted Commodity (name, price, age)", "3. Produce Commodity (name, price, expirationDate)", "Select: "};
		ArrayList<Integer> numlist = new ArrayList<>();
		boolean flag = true;
		int type;
		do {
			for(String p: prompts) {
				System.out.println(p);
			}
			try {
				type = Integer.parseInt(input.nextLine());
				Commodity c = initCommodity(input, type);
				for(int ind = 0; ind < s.getStoreSize(); ind++) {
					Commodity ssCommodity = s.getStores().get(ind);
					if(c.equals(s.getStores().get(ind))) {
						numlist.add(ind);
					}
				}
				return numlist;
			}catch(NumberFormatException e) {
				System.out.print("please check your input and input again\n");
				flag = false;
			}
		}while(!flag);
		return numlist;
	}
	
	//compute function
	public static double computePrice(Store s) {
		return s.computePrice();
	}
	
	// menu function
	public static Store menu(Store s) {
		String[] prompt = {"\t\t\tStore Management System v1.0\t\t\t\n",
							"1.add a commodity",
							"2.sale a commodity",
							"3.find a commodity",
							"4.modify one commodity's name or price",
							"5.compute commodities' price",
							"6.show commodities message",
							"7.exit",
							"please input your choice:"};
		
		Scanner input = new Scanner(System.in);
		boolean flag = true;
		do {
			for (String p: prompt) {
				System.out.println(p);
			}
			String c = input.nextLine();
			switch (c) {
				case "1":  // add function
					s = addCommodity(s, input);
					break;
				case "2": // delete function
					s = saleCommodity(s, input);
					if(s.isSaled()) {
						System.out.println("sale commodities info: ");
						for(int i=0; i<s.getSaledStores().size(); i++) {
							System.out.println(s.getSaledStores().get(i).toFilestream());
						}
						double price = s.computePrice(s.getSaledStores());
						System.out.println("total price: %.2f".formatted(price) + " dollar .");
					}
					break;
				case "3": // find function
					ArrayList<Integer> indexes = findCommodity(s, input);
					if (indexes.size() == 0) {
						System.out.println("Sorry, no commodity found.");
					}else {
						System.out.print("They are :");
						for(int i: indexes) {
							System.out.println(i + " " + s.getStores().get(i).toFilestream());
						}
						System.out.println();
					}
					break;
				case "4": // modify function
					s = changeCommodity(s, input);
					break;
				case "5": //compute price function
					System.out.println("The sum of commodities' price is %.2f".formatted(s.computePrice()) + " dollar .");
					break;
				case "6": // show commodity message function
					s.showFileStream();
					break;
				case "7": // exit
					flag = false;
					System.out.println("Goodbye, ^_^");
					break;
				default:
					System.out.println("Sorry, it not support; please choose again");
			}
		}while(flag);
		return s;
	}
	
	// main
	public static void main(String[] args) {
//		Store ssStore = readFile("./src/project/stock.csv");
		Store ssStore = readFile("./src/project/save.csv");
		ssStore = menu(ssStore);
		System.out.println("done");
		
		writeFile("./src/project/save.csv", ssStore);
	}

}
