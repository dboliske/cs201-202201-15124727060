// name: ShaoQiang Guo ;	 course: CS201;	sec: 03
// date: April. 25, 2022; 	 name of project: labs.project
package project;

import java.util.Scanner;

public class AgeRestrictedCommodity extends Commodity {
	private int age;
	
	// default constructor
	public AgeRestrictedCommodity() {
		super();
		age = 0;
	}
	
	// non-default constructor
	public AgeRestrictedCommodity(String name, double price, int age) {
		super(name, price);
		this.age = 0;
		setAge(age);
	}
	
	// acessor method for age
	public int getAge() {
		return age;
	}
	
	// mutator method for age
	public void setAge(int age) {
		if (age > 0) {
			this.age = age;
		}else {
			super.setInit(false);
		}
	}
	
	// vaild agerestricted method
	public boolean vaildAgeRestricted(int age) {
		if(age <= 0) {
			return false;
		}else if(this.age > age) {
			return false;
		}
		return true;
	}
	
	// toString
	@Override
	public String toString() {
		return "It's " + super.getName() + " and its price is " + 
				super.getPrice() + "; cannot sale to be sold to people under "+ age + " years old. ";
	}
	
	// equals method
	@Override
	public boolean equals(Object obj) {
		if(!(super.equals(obj))) {
			return false;
		}else if (!(obj instanceof AgeRestrictedCommodity)) {
			return false;
		}
		
		AgeRestrictedCommodity ac = (AgeRestrictedCommodity)obj;
		if(this.age != ac.age) {
			return false;
		}
		return true;
	}
	
	// allowSaled method
	@Override
	public boolean allowSaled() {
		Scanner input = new Scanner(System.in);
		boolean flag = false;
		int age = 0;
		do {
			System.out.println("Please input customer's age: ");
			try {
				age = Integer.parseInt(input.nextLine());
			} catch(Exception e) {
				System.out.println("Wrong data format,lease check your input and input again");
				flag = true;
			}
		}while(flag);
		return this.age > age ? true: false;
	}
	
	// allowSaled method with age param
	public boolean allowSaled(int age) {
		return this.age > age ? true: false;
	}
	
	// filestream method
	@Override
	public String toFilestream() {
		return super.toFilestream() + "," + age;
	}
}
